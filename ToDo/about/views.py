from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse

# Create your views here.

def about (request):
    return render (request,'about/about.html')