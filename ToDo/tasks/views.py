from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from .models import Task
from .forms import TaskForm
from django.contrib import messages
from django.core.paginator import Paginator
import datetime

@login_required
def tasksList(request):
    search = request.GET.get('search')
    filter = request.GET.get('filter')

    if search:
        tasks = Task.objects.filter(title__icontains=search, user=request.user)
    
    elif filter:
        if filter == 'all':
            tasks_list = Task.objects.all().order_by('-created_at').filter(user=request.user)

            paginator = Paginator(tasks_list, 3)
            page = request.GET.get('page')
            tasks = paginator.get_page(page)
        
        else:
            tasks = Task.objects.filter(done=filter, user=request.user)
            
    else:
        tasks_list = Task.objects.all().order_by('-created_at').filter(user=request.user)

        paginator = Paginator(tasks_list, 3)
        page = request.GET.get('page')
        tasks = paginator.get_page(page)
        
    return render(request,'tasks/list.html',{'tasks': tasks})

@login_required
def taskDetails(request, id):
    task = get_object_or_404(Task, pk=id, user=request.user)
    return render (request, 'tasks/task-details.html', {'task': task})

@login_required
def newTask(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        
        if form.is_valid():
            task = form.save(commit=False)
            task.done = 'doing'
            task.user = request.user
            task.save()
            return redirect('/')
    else:
        form = TaskForm()
        return render (request, 'tasks/new-task.html', {'form': form})

def updateTask(request,id):
    task = get_object_or_404(Task, pk=id)
    form = TaskForm(instance=task)

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)

        if form.is_valid():
            task.save()
            return redirect('/')
        else:
            return render(request, 'tasks/update-task.html', {'form': form, 'task': task })
    else:
        return render(request, 'tasks/update-task.html', {'form': form, 'task': task })

@login_required
def deleteTask(request,id):
    task = get_object_or_404(Task, pk=id)
    task.delete()
    messages.info(request,"Task successfully deleted")
    return redirect('/')

@login_required
def changesStatus(request, id):
    task = get_object_or_404(Task, pk=id)

    if task.done == 'doing':
        task.done = 'done'
    else:
        task.done = 'doing'
    
    task.save()

    return redirect('/')

@login_required
def reportTasks(request):
    taskDoneRecently = Task.objects.filter(done='done',updated_at__gt=datetime.datetime.now()-datetime.timedelta(days=30),user=request.user).count()
    tasksDone = Task.objects.filter(done='done', user=request.user).count()
    tasksDoing = Task.objects.filter(done='doing', user=request.user).count()

    return render(request,'tasks/report.html', 
        {'taskDoneRecently':taskDoneRecently,'tasksDone': tasksDone, 'tasksDoing':tasksDoing})


def ToDo(request):
    return HttpResponse('Hello World')

def yourName(request,name):
    return render(request, 'tasks/yourname.html', {'name': name})

def about (request):
    return render (request,'about/about.html')