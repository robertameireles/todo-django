$( document ).ready(function() {

    let baseUrl   = 'http://localhost:8000/';
    let searchBtn = $('#search-btn');
    let searchForm = $('#search-form');
    let filter     = $('#filter');
    
    $(searchBtn).on('click', function() {
        searchForm.submit();
    });

    $(filter).change(function() {
        let filter = $(this).val();
        window.location.href = baseUrl + '?filter=' + filter;
    });

});