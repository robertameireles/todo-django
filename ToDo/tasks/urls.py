from django.urls import path
from . import views


urlpatterns = [
    path('',views.tasksList,name='tasks-list'),
    path('tasks/<int:id>', views.taskDetails, name='task-details'),
    path('newtask/', views.newTask, name='task-new'),
    path('updatetask/<int:id>', views.updateTask, name='task-update'),
    path('changesStatus/<int:id>',views.changesStatus, name='change-status'),
    path('deletetask/<int:id>', views.deleteTask, name='delete-task'),
    path('report', views.reportTasks, name='report-tasks'),
    path('about/', views.about, name='about'),
    path('todo/', views.ToDo),
    path('yourname/<str:name>',views.yourName, name='your-name'),
]
